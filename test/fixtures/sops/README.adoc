= SOPS Test Fixtures

These files are used to test SOPS/age encryption utilities.

key.txt::
  A unique age key used to encrypt files in this directory.
  (obviously not a key used elsewhere/in production)

data.yml::
SOPS secured example data.
Contains `example` (`"foo"`),
and `example_secure` (`"bar"`).
+
Edit with:
+
[code,sh]
----
cd test/fixtures/sops/
SOPS_AGE_KEY_FILE=key.txt EDITOR="code --wait" sops data.yml
----
